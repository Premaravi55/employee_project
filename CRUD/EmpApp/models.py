from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class User(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, blank=True)
    name = models.CharField(max_length=200, null=True)
    email = models.CharField(max_length=200, null=True)

    def __str__(self):
        return self.name


class Employee(models.Model):
    eid = models.AutoField(primary_key=True)
    Name = models.CharField(max_length=30)
    Address = models.CharField(max_length=30)
    Gender = models.CharField(max_length=15)
    joined_Date = models.DateField()
    created_Time = models.TimeField(verbose_name='Created on', auto_now_add=True, help_text='Data created on')
    updated_time = models.TimeField(verbose_name='Updated on', auto_now_add=True, help_text='Data updated on')

    class Meta:
        db_table = 'Employee'


class Salary(models.Model):
    sid = models.AutoField(primary_key=True)
    employee_id = models.ForeignKey(Employee, on_delete=models.CASCADE, max_length=15)
    salary = models.IntegerField()
    month = models.CharField(max_length=20)
    created_Time = models.DateTimeField(verbose_name='Created on', auto_now_add=True, help_text='Data created on')
    updated_time = models.DateTimeField(verbose_name='Updated on', auto_now_add=True, help_text='Data updated on')
