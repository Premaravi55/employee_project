from django import forms
from .models import Employee, Salary


class EmployeeForm(forms.ModelForm):
    class Meta:
        model = Employee
        fields = ('Name', 'Address', 'Gender', 'joined_Date')


class EmployeeUpdateForm(forms.ModelForm):
    class Meta:
        model = Employee
        fields = ('Name', 'Address', 'Gender')



