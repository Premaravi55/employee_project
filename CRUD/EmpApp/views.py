from django.shortcuts import render, redirect
from .models import Employee, Salary, User
from .forms import EmployeeForm, EmployeeUpdateForm


# Create your views here.
def welcome(request):
    return render(request, 'welcome.html')


def load_form(request):
    form = EmployeeForm
    return render(request, 'index.html', {'form': form})


def add(request):
    form = EmployeeForm(request.POST)
    print(form.is_valid())
    if form.is_valid():
        form.save()
    else:
        print(form.errors)
    return redirect('show')


def show(request):
    employee = Employee.objects.all()
    return render(request, 'show.html', {'employee': employee})


def edit(request, id):
    employee = Employee.objects.get(eid=id)
    form = EmployeeUpdateForm(employee)
    print(form.data.Name)
    return render(request, 'edit.html', {'form': form})


def update(request, id):
    employee = Employee.objects.get(eid=id)
    form = EmployeeUpdateForm(request.POST, instance=employee)
    if form.is_valid():
        form.save()
    else:
        print(form.errors)
    return redirect('show')


def delete(request, id):
    employee = Employee.objects.get(eid=id)
    employee.delete()
    return redirect('show')


def search(request):
    given_name = request.POST['name']
    employee = Employee.objects.filter(Name=given_name)
    return render(request, 'show.html', {'employee': employee})
